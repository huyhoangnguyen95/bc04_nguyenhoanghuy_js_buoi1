/*
Input :
Nhập vào 5 số thực
1.2
2.2
3.3
4.5
6

To do : (1.2 + 2.2 + 3.3 + 4.5 + 6)/5

Output :
3.44

*/

var a = 1.2;
var b = 2.2;
var c = 3.3;
var d = 4.5;
var e = 6;
var result = (a + b + c + d + e)/5;
console.log('result: ', result);